#pragma once
#include <iostream>
#include<cstdio>
#include  <string>
using namespace std;



struct avl_tree {
	int co;
	avl_tree* rodzic;
	avl_tree* lewe;
	avl_tree* prawe;
};

avl_tree* ll_rotacja(avl_tree *A);
avl_tree* lr_rotacja(avl_tree *A);
avl_tree* rl_rotacja(avl_tree *A);
avl_tree* rr_rotacja(avl_tree *A);
void wyswietl(avl_tree *A, int gleb);
/*
avl_tree* ll_rotacja(avl_tree *A) {
	avl_tree *pom=A->lewe;
	A->lewe = pom->prawe;
	pom->prawe = A;
	return pom;
}

avl_tree* lr_rotacja(avl_tree *A) {
	avl_tree *pom = A->lewe;
	A->lewe = rr_rotacja(pom);
	return ll_rotacja(A);
}

avl_tree* rr_rotacja(avl_tree *A) {
	avl_tree *pom = A->prawe;
	A->prawe = pom->lewe;
	pom->lewe = A;
	return pom;
}

avl_tree* rl_rotacja(avl_tree *A) {
	avl_tree *pom=A->prawe;
	A->prawe = ll_rotacja(pom);
	return rr_rotacja(A);
}
*/


int wys(avl_tree* A) {
	if (A == nullptr) { return 0; }
	int lewa_w = wys(A->lewe);
	int prawa_w = wys(A->prawe);

	return(lewa_w > prawa_w) ? lewa_w + 1 : prawa_w + 1;
}

int wys_min(avl_tree *A) {
	if (A == nullptr) { return 0; }
	int lewa_w = wys_min(A->lewe);
	int prawa_w = wys_min(A->prawe);

	return(lewa_w < prawa_w) ? lewa_w +1 : prawa_w +1;
}

int roznica(avl_tree *A) {
	return (wys(A->lewe) - wys(A->prawe));
}

avl_tree* balans(avl_tree *A) {//Sprawdza z ktorej strony jest przeciazenie i wywoluje odpowiednia funkcje rotacyjna
	int rozn = roznica(A);
	if (rozn > 1 || rozn<-1) {
		cout << "galaz: \n";
		wyswietl(A, 0);
	}
	if (rozn > 1) {
		if (roznica(A->lewe) > 0)
			A = ll_rotacja(A);
		else
			A = lr_rotacja(A);
	}
	else if (rozn < -1) {
		if (roznica(A->prawe) > 0)
			A = rl_rotacja(A);
		else
			A = rr_rotacja(A);
	}
	if (rozn > 1 || rozn < -1) {
		cout << "zostala przeksztalcona na : \n";
		wyswietl(A, 0);
	}
	return A;
}

avl_tree* znajdz_min(avl_tree* A) {
	while (A->lewe != nullptr) { A = A->lewe; }
	return A;
}

void dodaj(avl_tree *&A, int we) {
	if (A == nullptr) {
		A = new avl_tree();
		A->lewe = A->prawe = nullptr;
		A->co = we;
		A->rodzic = nullptr;
		return;
	}
	else {
		if (we > A->co) { dodaj(A->prawe, we); A->prawe->rodzic = A; A = balans(A); }
		if (we <= A->co) {
			dodaj(A->lewe, we); 
			A->lewe->rodzic = A;
			A = balans(A);
		}
	}
}

void kasuj(avl_tree *A) {
	if (A)
	{
		kasuj(A->lewe);   // usuwamy lewe poddrzewo
		kasuj(A->prawe);  // usuwamy prawe poddrzewo
		delete A;              // usuwamy sam w�ze�
	}
}


void wyswietl(avl_tree *A, int gleb) {
	if (A != nullptr)
	{
		for (int i(0); i < gleb; i++)
			cout << "   ";
		cout << A->co << endl;

		wyswietl(A->lewe, gleb + 1);
		wyswietl(A->prawe, gleb + 1);
	}
	//cout << endl;
}

void wysw_z_rod(avl_tree *A) {
	if (A != nullptr) {
		cout << " wartosc:  " << A->co << " rodzic: ";
		if (A->rodzic != nullptr)
			cout << A->rodzic->co << endl;
		wysw_z_rod(A->lewe); wysw_z_rod(A->prawe);
	}
}

avl_tree* usun(avl_tree* A, int do_usun) {
	if (A == nullptr) return A;//puste
	else if (A->co > do_usun) A->lewe = usun(A->lewe, do_usun);//szuka w mniejszych wartosciach
	else if (A->co < do_usun) A->prawe = usun(A->prawe, do_usun);//szuka w wiekszych wartosciach
	else {
		if (A->lewe == nullptr && A->prawe == nullptr) {//nie ma dziecka
			avl_tree* pom = A;
			/*delete A;*/A = nullptr;	pom->rodzic = balans(pom->rodzic); return A;
		}
		else {//ma dzieci
			if (A->lewe == nullptr) {//nie ma lewego dziecka
				avl_tree* pom = A;
				A = A->prawe;
				pom->rodzic = balans(pom->rodzic);
				delete pom;
			}
			else if (A->prawe == nullptr) {//nie ma prawego dziecka
				avl_tree* pom = A;
				A = A->lewe;
				pom->rodzic = balans(pom->rodzic);
				delete pom;
			}
			else {//ma dwojke dzieci
				avl_tree* pom = znajdz_min(A->prawe);//zamienia z prawa najmniejsza
				A->co = pom->co;
				pom->rodzic = balans(pom->rodzic);
				A->prawe = usun(A->prawe, pom->co);//usuwa powtorke w prawej
			}
		}
	}
	return A;
}

avl_tree* ll_rotacja(avl_tree *A) {
	avl_tree *pom = A->lewe;
	pom->rodzic = A->rodzic;
	A->lewe = pom->prawe;
	if (A->lewe != nullptr)
		A->lewe->rodzic = A;
	A->rodzic = pom;
	pom->prawe = A;
	return pom;
}

avl_tree* lr_rotacja(avl_tree *A) {
	avl_tree *pom = A->lewe;
	A->lewe = rr_rotacja(pom);
	return ll_rotacja(A);
}

avl_tree* rr_rotacja(avl_tree *A) {
	avl_tree *pom = A->prawe;
	pom->rodzic = A->rodzic;
	A->prawe = pom->lewe;
	if(A->prawe!=nullptr)
		A->prawe->rodzic = A;
	A->rodzic = pom;
	pom->lewe = A;
	return pom;
}

avl_tree* rl_rotacja(avl_tree *A) {
	avl_tree *pom = A->prawe;
	A->prawe = ll_rotacja(pom);
	return rr_rotacja(A);
}
