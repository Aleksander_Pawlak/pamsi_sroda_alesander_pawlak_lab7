#pragma once
#include <list>
#include <iostream>
#include <ctime>
#include <math.h>
using namespace std;

class Tab_hasz
{
	list<int>* tab;
	int wielk;
	int wsp_a, wsp_b;
public:
	Tab_hasz(int ile);
	~Tab_hasz();
	void dodaj(int we);
	void usun(int klucz);
	int haszuj(int zmien);
	int szukaj(int co);
	void wyswietl();
};

