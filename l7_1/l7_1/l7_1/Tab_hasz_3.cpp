#include "stdafx.h"
#include "Tab_hasz_3.h"


Tab_hasz_3::Tab_hasz_3(int ile): Tab_hasz_2(ile){
	/*srand(time(NULL));
	bool czy(false);
	Tab_hasz_2::wielk = ile;
	Tab_hasz_2::tab = new int[ile];
	Tab_hasz_2::czy_wolne = new bool[ile];
	do {
		Tab_hasz_2::wsp_a = rand();
		Tab_hasz_2::wsp_b = rand() % (ile / 2);
		if (Tab_hasz_2::wsp_a%ile != 0) czy = true;
	} while (czy != true);*/
}


Tab_hasz_3::~Tab_hasz_3() {}

void Tab_hasz_3::dodaj(int we)
{
	int i = haszuj(we);
	int h=i, prob(0);
	while (true) {
		if (Tab_hasz_2::czy_wolne[i]) {
			Tab_hasz_2::tab[i] = we;
			Tab_hasz_2::czy_wolne[i] = false;
			break;
		}
		if (Tab_hasz_2::tab[i] == we)  break; 
		i = (i + 1) % Tab_hasz_2::wielk;
		prob++;
		if (i == h) {
			cout << "Brak wolnych miejsc w tablicy\n"; break;
		}
	}
	cout << "Potrzebne bylo " << prob << " probek zeby dodac wartosc\n";
}

int Tab_hasz_3::haszuj(int zmien)
{
	int tmp(0);
	int adres(0);
	int dk(0);
	bool czy(false);
	srand(time(NULL));
	do {
		tmp = rand() % wielk;
		czy = Tab_hasz_2::czy_pierwsz(tmp);
	} while (!czy);
	dk = tmp - (abs(zmien)%tmp);
	adres = (Tab_hasz_2::haszuj(zmien) + dk)% Tab_hasz_2::wielk;
	return adres;
}

int Tab_hasz_3::szukaj(int co)
{
	int gdzie(haszuj(co));
	int h = gdzie - 1, znalazl(0);
	do {
		if (Tab_hasz_2::tab[gdzie] == co && gdzie != h) {
			znalazl = 1;
		}
		else {
			if (gdzie != h) {
				gdzie++;
				gdzie = gdzie%Tab_hasz_2::wielk;
			}
			else {
				znalazl = 3;
			}
		}
	} while (znalazl == 0);

	if (znalazl == 3) {
		cout << "Nie znaleziono wartosci w tablicy, zwrocona zostanie wartosc 0\n";
		gdzie = 0;
	}
	return gdzie;
}

void Tab_hasz_3::usun(int klucz)
{
	int gdzie(haszuj(klucz));
	int h = gdzie - 1, znalazl(0);
	do {
		if (Tab_hasz_2::tab[gdzie] == klucz && gdzie != h) {
			znalazl = 1;
		}
		else {
			if (gdzie != h) {
				gdzie++;
				gdzie = gdzie%Tab_hasz_2::wielk;
			}
			else {
				znalazl = 3;
			}
		}
	} while (znalazl == 0);

	if (znalazl == 1) {
		Tab_hasz_2::tab[gdzie] = NULL;
		Tab_hasz_2::czy_wolne[gdzie] = true;
	}
	else
		cout << "Nie ma takiej wartosci w tablicy\n";
}
