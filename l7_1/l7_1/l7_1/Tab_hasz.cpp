#include "stdafx.h"
#include "Tab_hasz.h"

using namespace std;

Tab_hasz::Tab_hasz(int ile)
{
	srand(time(NULL));
	bool czy(false); 
	wielk = ile;
	tab = new list<int>[ile];
	do {
		wsp_a = rand();
		wsp_b = rand()%(ile/2);
		if (wsp_a%ile != 0) czy = true;
	} while (czy != true);
	//cout << "a: " << wsp_a << "\nb:  " << wsp_b << endl;
}


Tab_hasz::~Tab_hasz()
{
	delete[] tab;
}

void Tab_hasz::usun(int klucz)
{
	tab[szukaj(klucz)].remove(klucz);
}

int Tab_hasz::haszuj(int zmien)
{
	int adres(0);
	adres = (wsp_a*abs(zmien) + wsp_b) % wielk;
	return adres;
}

void Tab_hasz::dodaj(int we)
{
	tab[haszuj(we)].push_back(we);
}

int Tab_hasz::szukaj(int co)
{
	int gdzie(0),i=haszuj(co),flag(0);

	for (list<int>::iterator j = tab[i].begin(); j != tab[i].end(); ++j) {
		if (*j == co) { gdzie = i; flag = 1; }
	}
	if (flag == 0)
		cout << "wartosc nie zostala znaleziona, zwrocona zostanie wartosc 0\n";
	return gdzie;
}

void Tab_hasz::wyswietl()
{
	for (int i(0); i < wielk; i++) {
		cout << "komorka " << i << " w tablicy: ";
		for (list<int>::iterator j = tab[i].begin(); j != tab[i].end();j++) {
			cout << *j << "  ";
		}
		cout << endl;
	}
}
