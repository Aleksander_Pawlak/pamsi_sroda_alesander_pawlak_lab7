#pragma once
#include "Tab_hasz_2.h"
class Tab_hasz_3 :
	public Tab_hasz_2
{
public:
	Tab_hasz_3(int ile);
	~Tab_hasz_3();
	void dodaj(int we);
	int haszuj(int zmien);
	int szukaj(int co);
	void usun(int klucz);
};

