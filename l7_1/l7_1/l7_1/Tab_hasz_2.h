#pragma once
#include <iostream>
#include <ctime>
#include <math.h>

using namespace std;

class Tab_hasz_2
{
protected:
	int* tab;
	bool* czy_wolne;
	int wielk, wsp_a, wsp_b;
public:
	Tab_hasz_2(int ile);
	~Tab_hasz_2();
	virtual void dodaj(int we);
	virtual void usun(int klucz);
	virtual int haszuj(int zmien);
	virtual int szukaj(int co);
	void wyswietl();
	bool czy_pierwsz(int liczba);
};

