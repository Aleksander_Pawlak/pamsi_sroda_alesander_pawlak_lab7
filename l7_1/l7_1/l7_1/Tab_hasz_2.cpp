#include "stdafx.h"
#include "Tab_hasz_2.h"



Tab_hasz_2::Tab_hasz_2(int ile)
{
	srand(time(NULL));
	bool czy(false);
	wielk = ile;
	tab = new int[ile];
	czy_wolne = new bool[ile];
	do {
		wsp_a = rand();
		wsp_b = rand() % (ile / 2);
		if (wsp_a%ile != 0 && (wsp_a > 0 && wsp_b > 0)) czy = true;
	} while (czy != true);
}


Tab_hasz_2::~Tab_hasz_2()
{
	delete[] tab;
	delete[] czy_wolne;
}

void Tab_hasz_2::dodaj(int we)
{
	int i = haszuj(we);
	int h = i, prob(0);
	while (true) {
		if (Tab_hasz_2::czy_wolne[i]) {
			Tab_hasz_2::tab[i] = we;
			Tab_hasz_2::czy_wolne[i] = false;
			break;
		}
		if (Tab_hasz_2::tab[i] == we)  break;
		i = (i + 1) % Tab_hasz_2::wielk;
		prob++;
		if (i == h) {
			cout << "Brak wolnych miejsc w tablicy\n"; break;
		}
	}
	cout << "Potrzebne by�o " << prob << " probek\n";
}


void Tab_hasz_2::usun(int klucz)
{
	int gdzie(haszuj(klucz));
	int h = gdzie - 1, znalazl(0);
	do {
		if (tab[gdzie] == klucz && gdzie != h) {
			znalazl = 1;
		}
		else {
			if (gdzie != h) {
				gdzie++;
				gdzie = gdzie%wielk;
			}
			else {
				znalazl = 3;
			}
		}
	} while (znalazl == 0);

	if (znalazl == 1) {
		tab[gdzie] = NULL;
		czy_wolne[gdzie] = true;
	}
	else
		cout << "Nie ma takiej wartosci w tablicy\n";
}

int Tab_hasz_2::haszuj(int zmien)
{
	int adres(0);
	adres = (wsp_a*abs(zmien) + wsp_b) % wielk;
	return adres;
}


int Tab_hasz_2::szukaj(int co)
{
	int gdzie(haszuj(co));
	int h = gdzie - 1, znalazl(0);
	do {
		if (tab[gdzie] == co && gdzie != h) {
			znalazl = 1;
		}
		else {
			if (gdzie != h) {
				gdzie++;
				gdzie = gdzie%wielk;
			}
			else {
				znalazl = 3;
			}
		}
	} while (znalazl==0);

	if (znalazl == 3) {
		cout << "Nie znaleziono wartosci w tablicy, zwrocona zostanie wartosc 0\n";
		gdzie = 0;
	}
	return gdzie;
}

void Tab_hasz_2::wyswietl()
{
	for (int i(0); i < wielk; i++) {
		if (!czy_wolne[i])
			cout << "tab[" << i << "] :  " << tab[i] << endl;
		else
			cout << "tab[" << i << "] pusta\n";
	}
}

bool Tab_hasz_2::czy_pierwsz(int liczba)
{
	if (liczba<2)
		return false;

	for (int i = 2; i*i <= liczba; i++)
		if (liczba%i == 0)
			return false;
	return true;
}
