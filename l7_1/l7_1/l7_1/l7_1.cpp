// l7_1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstdlib>
#include <iostream>
#include <string>
#include "Tab_hasz.h"
#include "Tab_hasz_2.h"
#include "Tab_hasz_3.h"
#include <ctime>

using namespace std;

void menu();
void podmen1();
void podmen2();
void podmen3();
bool pierw(int liczba) {
	if (liczba<2)
		return false;

	for (int i = 2; i*i <= liczba; i++)
		if (liczba%i == 0)
			return false;
	return true;
}

bool ddd(int liczba) {
	if (liczba<2)
		return false;

	for (int i = 2; i*i <= liczba; i++)
		if (liczba%i == 0)
			return false;
	return true;
 }

void wyswietl_tab(int tab[],int ile) {
	for (int i(0); i < ile; i++) {
		if (i != 0 && i % 10 == 0)
			cout << endl;
		cout << tab[i] << "  ";
	}
	cout << endl;
}
int main()
{
	/*srand(time(NULL));
	int pom(0);
	pom = rand() % 13;
	cout <<pom<<boolalpha<< ddd(pom) << endl;*/
	menu();

	system("PAUSE");
    return 0;
}

void menu() {
	int wybor(0);
	do {
		cout << "Wybierz rodzaj tablicy haszujacej:\n1. Tablica z linkowaniem\n2. Tablilca z probkowaniem liniowym\n3. Tablica z podwojnym haszowaniem\n0. wyjscie\n";
		cin >> wybor;
		switch (wybor) {
		case 1: podmen1(); break;
		case 2: podmen2(); break;
		case 3: podmen3(); break;
		case 0: break;
		default: cout << "zly wybor\n"; break;
		}
	} while (wybor != 0);
}

void podmen1() {
	int wybor(0), ile, ile2, czy(0);
	int *tab;
	do{
		cout << "Podaj rozmiar tablicy: ";
	cin >> ile;
} while (!pierw(ile));
	Tab_hasz A(ile);
	srand(time(NULL));
	do {
		cout << "Wybierz operacje na tablicy haszujacej:\n1. wypelnij tablice losowymi elementami\n2. dodaj element\n3. znajdz indeks elementu\n4. usun element\n";
		cout<<"5. Wyswietl zawartosc calej tablicy\n0. wstecz\n";
		cin >> wybor;
		switch (wybor) {
		case 1: { cout << "ile liczb wylosowac i dodac do tablicy?\n"; cin >> ile; cout << "maksymalna wartosc liczb losowych: "; cin >> ile2; tab = new int[ile];
			for (int i(0); i < ile; i++)
				tab[i] = rand() % ile2;

			cout << "losowe liczby to:\n";
			wyswietl_tab(tab, ile);
			for (int j(0); j < ile; j++)
				A.dodaj(tab[j]);
			delete[] tab;
			break; }
		case 2: cout << "Podaj wartosc elementu:"; cin >> ile; A.dodaj(ile); break;
		case 3: cout << "Podaj wartosc elementu: "; cin >> ile; cout << "element znajduje sie w " << A.szukaj(ile) << " komorce tablicy\n"; break;
		case 4: cout << "Podaj wartosc elementu "; cin >> ile; A.usun(ile); break;
		case 5: A.wyswietl(); break;
		case 0: break;
		default: cout << "zly wybor\n"; break;
		}
	} while (wybor != 0);
}

void podmen2() {
	int wybor(0), ile, ile2, czy(0);
	int *tab;
	
	do {
		cout << "Podaj rozmiar tablicy: ";
		cin >> ile;
	} while (!pierw(ile));
	Tab_hasz_2 A(ile);
	srand(time(NULL));
	do {
		cout << "Wybierz operacje na tablicy haszujacej:\n1. wypelnij tablice losowymi elementami\n2. dodaj element\n3. znajdz indeks elementu\n4. usun element\n";
		cout << "5. Wyswietl zawartosc calej tablicy\n0. wstecz\n";
		cin >> wybor;
		switch (wybor) {
		case 1: { cout << "ile liczb wylosowac i dodac do tablicy?\n"; cin >> ile; cout << "maksymalna wartosc liczb losowych: "; cin >> ile2; tab = new int[ile];
			for (int i(0); i < ile; i++)
				tab[i] = rand() % ile2;

			cout << "losowe liczby to:\n";
			wyswietl_tab(tab, ile);
			for (int j(0); j < ile; j++)
				A.dodaj(tab[j]);
			delete[] tab;
			break; }
		case 2: cout << "Podaj wartosc elementu:"; cin >> ile; A.dodaj(ile); break;
		case 3: cout << "Podaj wartosc elementu: "; cin >> ile; cout << "element znajduje sie w " << A.szukaj(ile) << " komorce tablicy\n"; break;
		case 4: cout << "Podaj wartosc elementu "; cin >> ile; A.usun(ile); break;
		case 5: A.wyswietl(); break;
		case 0: break;
		default: cout << "zly wybor\n"; break;
		}
	} while (wybor != 0);
}

void podmen3() {
	int wybor(0), ile, ile2, czy(0);
	int *tab;
	do {
		cout << "Podaj rozmiar tablicy: ";
		cin >> ile;
	} while (!pierw(ile));
	Tab_hasz_3 A(ile);
	srand(time(NULL));
	do {
		cout << "Wybierz operacje na tablicy haszujacej:\n1. wypelnij tablice losowymi elementami\n2. dodaj element\n3. znajdz indeks elementu\n4. usun element\n";
		cout << "5. Wyswietl zawartosc calej tablicy\n0. wstecz\n";
		cin >> wybor;
		switch (wybor) {
		case 1: { czy = 1; cout << "ile liczb wylosowac i dodac do tablicy?\n"; cin >> ile; cout << "maksymalna wartosc liczb losowych: "; cin >> ile2; tab = new int[ile];
			for (int i(0); i < ile; i++)
				tab[i] = rand() % ile2;

			cout << "losowe liczby to:\n";
			wyswietl_tab(tab, ile);
			for (int j(0); j < ile; j++)
				A.dodaj(tab[j]);

			delete[] tab;
			break; }
		case 2: cout << "Podaj wartosc elementu:"; cin >> ile; A.dodaj(ile); break;
		case 3: cout << "Podaj wartosc elementu: "; cin >> ile; cout << "element znajduje sie w " << A.szukaj(ile) << " komorce tablicy\n"; break;
		case 4: cout << "Podaj wartosc elementu "; cin >> ile; A.usun(ile); break;
		case 5: A.wyswietl(); break;
		case 0: break;
		default: cout << "zly wybor\n"; break;
		}
	} while (wybor != 0);
}