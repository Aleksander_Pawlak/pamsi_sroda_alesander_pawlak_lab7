#pragma once
#include <list>
#include <iomanip>

using namespace std;
#pragma region Struktury_obiektow
template<typename Typ>
struct Vertexx {
	Typ value;
	int *poz;
	Vertexx() {};
	Vertexx(const Vertexx & A) { value = A.value; poz = A.poz; }
	~Vertexx() { poz = new int(1); delete poz; };
	Typ return_val() { return value; }
	Vertexx& operator =(const Vertexx & other) { this->value = other->value; 
	this->poz = other->poz; return *this; }
	//Vertexx* operator =(const Vertexx & other) { this->value = other->value; this->poz = other->poz; return this; }
};

template<typename Typ>
bool operator ==(const Vertexx<Typ> &A, const Vertexx<Typ> &B) {
	return((A.value == B.value && A.poz == B.poz) || (A.value == B.value)) ? true : false;
}

template<typename Typ>
ostream& operator <<(ostream& stream, const Vertexx<Typ> &A) {
	stream << A.value;
	return stream;
}


template<typename Typ>
void dodaj_vert(Vertexx<Typ> &A, Typ we, int n) {
	if (A == nullptr) {
		A->value = we;
		A->poz = new int(n);
	}
}

template<typename Typ, typename Key>
struct Edgee {
	Key title;
	Vertexx<Typ> *pocz, *kon;
	int* poz;
	Edgee() { pocz = nullptr; kon = nullptr; };
	Edgee(Key tit) :title(tit) {};
	//Edgee(Key tit, Typ i, Typ e, int k) :title(tit) { poz = new int(k); pocz = new Vertexx<Typ>(i);  kon = new Vertexx<Typ>(e); };
	Edgee(Key tit, Vertexx<Typ> *A, Vertexx<Typ> *B, int i);
	~Edgee() { poz = new int(1); delete poz; };
	Typ valpocz() { Typ tmp = (*pocz).return_val(); return tmp; };
	Typ valkon() { Typ tmp = (*kon).return_val(); return tmp; };
	//Edgee& operator =(const Edgee & other) { this->title = other->title; this->poz = other->poz; this->pocz = other->pocz; this->kon = other->kon; return *this; }
};


template<typename Typ, typename Key>
bool operator ==(const Edgee<Typ, Key> &A, const Edgee<Typ, Key> &B) {
	return((A.title == B.title && A.pocz == B.pocz && A.kon == B.kon) || (A.title == B.title)) ? true : false;
}

template<typename Typ, typename Key>
ostream& operator <<(ostream& stream, const Edgee<Typ, Key> &A) {
	stream << "Polaczenie: " << A.title << " od: " << A.pocz << " do:  " << A.kon;
	return stream;
}
template<typename Typ, typename Key>
inline Edgee<Typ, Key>::Edgee(Key tit, Vertexx<Typ> *A, Vertexx<Typ> *B, int i)
{
	title = tit;
	pocz = &A;
	kon = &B;
	poz = new int(i);
}


#pragma endregion



template<typename Typ, typename Key>
class Graf_macierz
{
	list<Vertexx<Typ>> list_vertex;
	Edgee<Typ,Key> **macierz_sasiad;
	int size;
	int ile;
public:
	Graf_macierz(int wielk);
	~Graf_macierz();
	///uaktualniajace
	void insertVertex(Typ o);
	void insertEdge(Typ v, Typ w, Key o);
	void removeVertex(Typ o);
	void removeEdge(Key e);
	///iter
	void vertices();
	void edges();
};

template<typename Typ, typename Key>
Graf_macierz<Typ, Key>::Graf_macierz(int wielk)
{
	macierz_sasiad = new Edgee<Typ, Key>* [wielk];
	for (int i(0); i < wielk; i++)
		macierz_sasiad[i] = new Edgee<Typ, Key>[wielk];
	size = wielk;
	ile = 0;
}

template<typename Typ, typename Key>
Graf_macierz<Typ, Key>::~Graf_macierz()
{
	for (int i(0); i < size; i++)
		delete[] macierz_sasiad[i];
	delete[] macierz_sasiad;
}

template<typename Typ, typename Key>
inline void Graf_macierz<Typ, Key>::insertVertex(Typ o)
{
	Vertexx<Typ> We;
	auto it = distance(list_vertex.begin(), list_vertex.end());
	dodaj_vert(We, o, it+1);
	list_vertex.push_back(We);
}

template<typename Typ, typename Key>
inline void Graf_macierz<Typ, Key>::insertEdge(Typ v, Typ w, Key o)
{
	Vertexx<Typ> *A, *B;
	bool a(false), b(false);
	for (auto it = list_vertex.begin(); it != list_vertex.end(); ++it) {
		if ((*it).value == v) {
			A = &(*it);
			a = true;
		}
		if ((*it).value == w) {
			B = &(*it);
			b = true;
		}
		}
	int i = ile;
	if (i != size)
		if (a && b) {
			Edgee<Typ, Key> Tmp(o, A, B, i);
			ile++;
			macierz_sasiad[(*A).poz][(*B).poz] = Tmp;
			macierz_sasiad[*(B.poz)][*(A.poz)] = Tmp;
		}
		else
			cout << "Brak odpowiednich krawedzi";
	else
			cout << "brak wolnego miejsca\n";
}

template<typename Typ, typename Key>
inline void Graf_macierz<Typ, Key>::removeVertex(Typ o)
{
	Vertexx<Typ> *tmp = nullptr;
	for (auto it = list_vertex.begin(); it != list_vertex.end(); ++it) {
		if ((*it).value == o)
			tmp = it;
	}
	list_vertex.remove(tmp);
	for (auto it = list_vertex.begin(); it != list_vertex.end(); ++it) {
		*(it->poz) = *(it->poz) - 1;
	}
	for (int i((*tmp).poz); i < size; i++) {
		for (int l(0); l < size; l++) {
			macierz_sasiad[i][l] = macierz_sasiad[i + 1][l];
			macierz_sasiad[l][i] = macierz_sasiad[l][i + 1];
			macierz_sasiad[i][l].poz--;
			macierz_sasiad[l][i].poz--;
		}
	}
	ile--;
}

template<typename Typ, typename Key>
inline void Graf_macierz<Typ, Key>::vertices()
{
	cout << "wypisuje wszystkie wierzchiolki:\n";
	int y(0);
	for (auto it = list_vertex.begin(); it != list_vertex.end(); ++it) {
		if (y != 0 && y % 11 == 0) cout << endl;
		cout << (*it).value << "  ";
		y++;
	}
	cout << endl;
}

template<typename Typ, typename Key>
inline void Graf_macierz<Typ, Key>::edges()
{
	for (int l(0); l < ile; l++) cout << setw(3) << l;
	cout << endl << endl;
	for (int j(0); j <ile; j++) {
		cout << setw(3) << j;
		for (int k(0); k < ile; k++) {
			if (macierz_sasiad[j][k].title!="")
				cout << setw(3) << 1;
			else
				cout << setw(3) << 0;
		}
		cout << endl;
	}
}

