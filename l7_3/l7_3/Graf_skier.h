#pragma once
#include <iostream>
#include<list>
#include <vector>
#include <string.h>

using namespace std;

template<typename Typ>
class Graf_skier
{
	list<int> *sasiad;
	Typ *wartosci;
	int liczba;
	bool *tab_zaj;

public:
	Graf_skier(int ile);
	~Graf_skier();
	void dodaj(Typ co, int gdzie, vector<int> z_kim);
	void dodaj_sasiada(int kto, int z_kim);
	void usun(int adres);
	void wyswietl();
};

template<typename Typ>
Graf_skier<Typ>::Graf_skier(int ile)
{
	sasiad = new list<int>[ile];
	wartosci = new Typ[ile];
	liczba = ile;
	tab_zaj = new bool[ile];
}


template<typename Typ>
Graf_skier<Typ>::~Graf_skier()
{
	delete[] wartosci;
	delete[] sasiad;
	delete[] tab_zaj;
}

template<typename Typ>
void Graf_skier<Typ>::dodaj(Typ co,int gdzie, vector<int> z_kim) {
	wartosci[gdzie] = co;
	tab_zaj[gdzie] = false;
	int i(0);
	vector<int>::iterator it;
	for (it = z_kim.begin(); it != z_kim.end();++it) {
		sasiad[*it].remove(gdzie);
		sasiad[gdzie].push_back(*it);
	}
}

template<typename Typ>
inline void Graf_skier<Typ>::dodaj_sasiada(int kto, int z_kim)
{
	sasiad[kto].push_back(z_kim);
	sasiad[z_kim].remove(kto);
}

template<typename Typ>
inline void Graf_skier<Typ>::usun(int adres) {
	wartosci[adres] = nullptr;
	tab_zaj[adres] = true;
	sasiad[adres].clear();
}

template<typename Typ>
inline void Graf_skier<Typ>::wyswietl()
{
	for (int j(0); j < liczba; j++) {
		if (!tab_zaj[j]) {
			cout << "graf[" << j << "] = " << wartosci[j] << endl;
			cout << "Jest powiazany z wartoscia pod adresem: ";
			for (auto it = sasiad[j].begin(); it != sasiad[j].end(); ++it) {
				cout << *it << " , ";
			}cout << endl;
		}
		else {
			cout << "graf[" << j << "] pusty" << endl;;
		}
	}
	cout << endl;
}
