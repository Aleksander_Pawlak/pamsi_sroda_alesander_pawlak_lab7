// l7_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
//#include "Graf_skier.h"
//#include "Graf_skier_macierz.h"
#include <cstdlib>
#include "Graf_listy.h"
#include "Graf_macierz.h"
#include <algorithm>

using namespace std;
template<typename Typ, typename Key>
void menu();

struct Cipa {
	int co;
	int poz;
};

void dodaj(Cipa *&A,int we, int n) {
	A = new Cipa();
	A->co = we;
	A->poz = n;
}

struct Pepe {
	Cipa *A;
	int val;
	Pepe(int we, Cipa *We) :val(we) { A = We; };
};
int main()
{
	/*cout << "\a";
	//menu<int, string>();
	Cipa *B=nullptr;
	dodaj(B, 3, 1);
	Pepe P(0, B);
	cout << P.val << "  " << P.A->co << endl;*/

	Graf_macierz<int, string> A(5);
	A.insertVertex(21);
	A.insertVertex(9);
	A.insertEdge(9, 21, "dupa");
	A.insertEdge(1, 2, "kek");
	A.edges();
	A.vertices();

	system("PAUSE");
    return 0;
}

template<typename Typ,typename Key>
void menu()
{
	Graf_listy<Typ, Key> A;
	int wyb(0);
	Typ tmp1, tmp2; Key tmp3;
	Typ *tab;
	do {
		cout << "wybierz opcje\n1. dodaj wierzcholek\n2. Dodaj krawedz\n3. Usun Wierzcholek\n4. Usun krawedz\n5. Znajdz koncowe wierzcholki krawedzi\n6. Znajdz przeciwlegly wierzcholek wzgledem krawedzi\n";
		cout << "7. Sprawdz czy dwa wierzcholki ze soba sasiaduja\n8. Zamien wartosc w wierzcholku\n10. Wyswietl wszytskie wierzcholkow\n11. Wypisanie wszytskich krawedzi\n";
		cout << "0. koniec\n";
		cin >> wyb;
		switch (wyb) {
		case 1: {			cout << "Podaj wartosc wierzcholka:"; cin >> tmp1; A.insertVertex(tmp1); break;}
		case 2: {cout << "Podaj indeks krawedzi:\n"; cin >> tmp3; cout << " od ktorego wierzcholka ma byc?\n"; cin >> tmp1; cout << "do ktorego\n"; cin >> tmp2; 
			A.insertEdge(tmp1, tmp2, tmp3); break; }
		case 3: {cout << "ktory wierzcholek usunac?\n"; cin >> tmp1; A.removeVertex(tmp1); break; }
		case 4: {cout << "Ktora krawedz usunac?\n"; cin >> tmp3; A.removeEdge(tmp3); break; }
		case 5: { cout << "ktorej krawedzi?\n"; cin >> tmp3; tab = A.endVertices(tmp3); cout << "od: " << tab[0] << " do: " << tab[1] << endl; break; }
		case 6: {cout << "ktorego wierzcholka?\n"; cin >> tmp1; cout << "wzgledem ktorej krawedzi?\n"; cin >> tmp3; tmp2 = A.opposite(tmp1, tmp3); break; }
		case 7: {cout << "ktory?\n"; cin >> tmp1; cout << "z ktorym?\n"; cin >> tmp2; cout << boolalpha << A.areAdjacent(tmp1, tmp2) << endl; break; }
		case 8: {cout << "w ktorym wierzcholku: "; cin >> tmp1; cout << "Na jaka: "; cin >> tmp2; A.replace(tmp1, tmp2); }
		case 10: A.vertices(); break;
		case 11: A.edges(); break;
		case 0: break;
		default: cout << "zly wybor\n"; break;
		}
	} while (wyb != 0);
}
