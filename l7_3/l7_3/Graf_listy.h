#pragma once
#include <iostream>
#include <list>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;
#pragma region Struktury_obiektow
template<typename Typ>
struct Vertex {
	Typ value;
	int *poz;
	Vertex() {};
	Vertex(Typ in) :value(in) {};
	Vertex(Typ in, int i) :value(in) { poz = new int(i); };
	~Vertex() { poz = new int(1); delete poz;  };
	Typ return_val() { return value; }
	Vertex& operator =(const Vertex & other) { this->value = other->value; return *this; }
};

template<typename Typ>
bool operator ==(const Vertex<Typ> &A, const Vertex<Typ> &B) {
	return((A.value == B.value && A.poz==B.poz) || (A.value==B.value)) ? true : false;
}

template<typename Typ, typename Key>
ostream& operator <<(ostream& stream, const Vertex<Typ> &A) {
	stream << A.value;
	return stream;
}

template<typename Typ, typename Key>
struct Edge {
	Key title;
	Vertex<Typ> *pocz, *kon;
	int* poz;
	Edge(Key tit) :title(tit) {};
	Edge(Key tit, Typ i, Typ e, int k) :title(tit) {	poz = new int(k); pocz = new Vertex<Typ>(i);  kon = new Vertex<Typ>(e);	};
	~Edge() { poz = new int(1); delete poz; };
	Typ valpocz() {Typ tmp=(*pocz).return_val(); return tmp; };
	Typ valkon() { Typ tmp = (*kon).return_val(); return tmp; };
};


template<typename Typ, typename Key>
bool operator ==(const Edge<Typ, Key> &A, const Edge<Typ, Key> &B) {
	return((A.title == B.title && A.pocz==B.pocz && A.kon==B.kon) || (A.title==B.title)) ? true : false;
}

template<typename Typ, typename Key>
ostream& operator <<(ostream& stream, const Edge<Typ, Key> &A) {
	stream <<"Polaczenie: "<<A.title<< " od: " << A.pocz << " do:  " << A.kon;
	return stream;
}

#pragma endregion

template<typename Typ,typename Key>
class Graf_listy
{
	list<Edge<Typ, Key>> list_edges;
	list<Vertex<Typ>> list_vertex;
public:
	Graf_listy();
	~Graf_listy();
	///uaktualniajace
	void insertVertex(Typ o);
	void insertEdge(Typ v, Typ w, Key o);
	void removeVertex(Typ o);
	void removeEdge(Key e);
	///dostepu
	Typ* endVertices(Key e);
	Typ opposite(Typ v, Key e);
	bool areAdjacent(Typ v, Typ w);
	void replace(Typ v, Typ x);
	void replaceEdge(Key e, Key x);
	///iter
	void vertices();
	void edges();
};

template<typename Typ, typename Key>
Graf_listy<Typ,Key>::Graf_listy(){}

template<typename Typ, typename Key>
Graf_listy<Typ,Key>::~Graf_listy(){}

template<typename Typ, typename Key>
inline void Graf_listy<Typ, Key>::insertVertex(Typ o)
{
	auto it = distance(list_vertex.begin(), list_vertex.end());
	Vertex<Typ> tmp(o,it+1);
	list_vertex.push_back(tmp);
}

template<typename Typ, typename Key>
inline void Graf_listy<Typ, Key>::insertEdge(Typ v, Typ w, Key o)
{
	auto it = distance(list_edges.begin(), list_edges.end());
	Edge<Typ, Key> tmp(o, v, w, it + 1);
	list_edges.push_back(tmp);
}

template<typename Typ, typename Key>
inline void Graf_listy<Typ, Key>::removeVertex(Typ o)
{
	Vertex<Typ> tmp(o);
	list_vertex.remove(tmp);
	for (auto it = list_vertex.begin(); it != list_vertex.end(); ++it) {
		*(it->poz) = *(it->poz) - 1;
	}
}

template<typename Typ, typename Key>
inline void Graf_listy<Typ, Key>::removeEdge(Key e)
{
	Edge<Typ, Key> tmp(e);
	list_edges.remove(tmp);
	for (auto it = list_edges.begin(); it != list_edges.end(); ++it) {
		*(it->poz) = *(it->poz) - 1;
	}
}

template<typename Typ, typename Key>
inline Typ * Graf_listy<Typ, Key>::endVertices(Key e){
	Typ* tmp = new Typ[2];
	for (auto it = list_edges.begin(); it != list_edges.end(); ++it) {
		if (it->title == e) {
			tmp[0] = (*it).valpocz();
			tmp[1] = (*it).valkon();
		}
	}
	return tmp;
}

template<typename Typ, typename Key>
inline Typ Graf_listy<Typ, Key>::opposite(Typ v, Key e)
{
	Typ tmp;
	for (auto it = list_edges.begin(); it != list_edges.end(); ++it) {
		if (it->title == e) {
			if ((*it).valpocz() == v) tmp = (*it).valkon();
			else if((*it).valkon() ==v) tmp = (*it).valpocz();
			else { cout << "Ta krawedz nie styka sie z podanym wierzcholkiem\n"; }
		}
	}
	return tmp;
}

template<typename Typ, typename Key>
inline bool Graf_listy<Typ, Key>::areAdjacent(Typ v, Typ w)
{
	for (auto it = list_edges.begin(); it != list_edges.end(); ++it) {
		if (((*it).valpocz() == v && (*it).valkon() == w) || ((*it).valpocz() == w && (*it).valkon() == v))
			return true;
	}
	return false;
}

template<typename Typ, typename Key>
inline void Graf_listy<Typ, Key>::replace(Typ v, Typ x)
{
	bool f(false);
	for (auto it = list_vertex.begin(); it != list_vertex.end(); ++it) {
		if ((*it).value == v) {
			(*it).value = x;
			f = true;
		}
	}
	if (!f) {
		cout << "brak takiego wierzcholka\n";
	}
}

template<typename Typ, typename Key>
inline void Graf_listy<Typ, Key>::replaceEdge(Key e, Key x)
{

}

template<typename Typ, typename Key>
inline void Graf_listy<Typ, Key>::vertices()
{
	cout << "wypisuje wszystkie wierzchiolki:\n";
	int y(0);
	for (auto it = list_vertex.begin(); it != list_vertex.end(); ++it) {
		if (y != 0 && y % 11 == 0) cout << endl;
		cout << (*it).value << "  ";
		y++;
	}
	cout << endl;
}

template<typename Typ, typename Key>
inline void Graf_listy<Typ, Key>::edges()
{
	cout << "Wypisuje wszystkie krawedzie: \n";
	for (auto it = list_edges.begin(); it != list_edges.end(); ++it) {
		cout << "Polaczenie: " << (*it).title << " od: " << (*it).pocz->value << " do:  " << (*it).kon->value;
	}
}

