#pragma once
#include <iostream>
#include<string>
#include <iomanip>
#include<vector>

using namespace std;


template<typename Typ>
class Graf_skier_macierz
{
	bool* czy_wolne;
	Typ *wartosci;
	int **macierz_sasiad;
	int liczba;
public:
	Graf_skier_macierz(int ile);
	~Graf_skier_macierz();
	void dodaj(Typ co, int gdzie, vector<int> z_kim);
	void dodaj_sasiada(int kto, int z_kim);
	void usun(int adres);
	void wyswietl();
};

template<typename Typ>
Graf_skier_macierz<Typ>::Graf_skier_macierz(int ile)
{
	macierz_sasiad = new int*[ile];
	for (int y(0); y < ile; y++)
		macierz_sasiad[y] = new int[ile];
	liczba = ile;
	czy_wolne = new bool[ile];
	wartosci = new int[ile];
	for (int i(0); i < ile; i++)
		for (int j(0); j < ile; j++)
			macierz_sasiad[i][j] = 0;
}

template<typename Typ>
Graf_skier_macierz<Typ>::~Graf_skier_macierz()
{
	delete[] czy_wolne;
	delete[] wartosci;
	delete[] macierz_sasiad;
}

template<typename Typ>
void Graf_skier_macierz<Typ>::dodaj(Typ co, int gdzie, vector<int> z_kim){
	wartosci[gdzie] = co;
	czy_wolne[gdzie] = false;

	vector<int>::iterator it;
	for (it = z_kim.begin(); it != z_kim.end(); ++it) {
		macierz_sasiad[gdzie][*it] = 1;
		macierz_sasiad[*it][gdzie] = 0;
	}
}

template<typename Typ>
inline void Graf_skier_macierz<Typ>::dodaj_sasiada(int kto, int z_kim){
	macierz_sasiad[kto][z_kim] = 1;
	macierz_sasiad[z_kim][kto] = 0;
}

template<typename Typ>
inline void Graf_skier_macierz<Typ>::usun(int adres){
	wartosci[adres] = nullptr;
	czy_wolne[adres] = true;
	for (int i(0); i < liczba; i++)
		macierz_sasiad[adres][i] = 0;
}

template<typename Typ>
inline void Graf_skier_macierz<Typ>::wyswietl()
{
	for (int i(0); i < liczba; i++) {
		if (!czy_wolne[i])
			cout << "graf[" << i << "] = " << wartosci[i] << endl;
		else
			cout << "graf[" << i << "] pusty" << endl;
	}
	cout << "macierz korelacji ma postac:\n   ";
	for (int l(0); l < liczba; l++) cout << setw(3) << l;
	cout << endl << endl;
	for (int j(0); j < liczba; j++) {
		cout << setw(3) << j;
		for (int k(0); k < liczba; k++) {
			cout << setw(3) << int(macierz_sasiad[j][k]);
		}
		cout << endl;
	}
}


