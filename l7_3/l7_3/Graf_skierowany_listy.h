#pragma once
#include <iostream>
#include <list>
#include <vector>
#include <string.h>

using namespace std;
#pragma region struktury_zmiennych
template<typename Typ, typename Key>
struct Edge {
	Typ destiny;
	Key title;
	Edge(Typ in, Key in2) :destiny(in), title(in2) {};
	Edge(Typ in):destiny(in) {};
};

template<typename Typ, typename Key>
bool operator ==(const Edge<Typ, Key> &A, const Edge<Typ, Key> &B) {
	return(A.destiny == B.destiny) ? true : false;
}

template<typename Typ, typename Key>
ostream& operator <<(ostream& stream, const Edge<Typ, Key> &A) {
	stream << "do: " << A.destiny << " z indeksem:  " << A.title;
	return stream;
}


template<typename Typ, typename Key>
struct Zmienna {
	Typ value;
	list<Edge<Typ,Key>> edges;
	Zmienna() {};
	Zmienna(Typ in) :value(in) {};
	void Add_Edge(Typ in, Key in2) { Edge<Typ, Key> tmp(in, in2); edges.push_back(tmp); }
	void Erase_Edge(Typ in); 
	void display_val() { cout << value; }
	void display_edges() { cout << "krawedzie wartosci: " << value << " to:\n"; for (auto it = edges.begin(); it != edges.end(); ++it) { cout << *it << endl; } }
};

template<typename Typ, typename Key>
bool operator ==(const Zmienna<Typ, Key> &A, const Zmienna<Typ, Key> &B) {
	return(A.value == B.value) ? true : false;
}

template<typename Typ, typename Key>
inline void Zmienna<Typ, Key>::Erase_Edge(Typ in){
	Edge<Typ, Key> tmp(in); edges.remove(tmp);
}
#pragma endregion

template<typename Typ, typename key>
class Graf_skierowany_listy
{
	vector<Zmienna<Typ, key>> array_verdex;
public:
	Graf_skierowany_listy();
	~Graf_skierowany_listy();
	void InsertVerdex(Typ o);
	void InsertEdge(Typ v, key w, Typ o);
	void RemoveVerdex(Typ o);
	void RemoveEdge(key e);
};

template<typename Typ, typename key>
Graf_skierowany_listy<Typ, key>::Graf_skierowany_listy(){}

template<typename Typ, typename key>
Graf_skierowany_listy<Typ, key>::~Graf_skierowany_listy()
{
}

template<typename Typ, typename key>
inline void Graf_skierowany_listy<Typ, key>::InsertVerdex(Typ o)
{
	Zmienna tmp(o);
	array_verdex.push_back(tmp);
}

template<typename Typ, typename key>
inline void Graf_skierowany_listy<Typ, key>::InsertEdge(Typ v, key w, Typ o)
{
	Zmienna<Typ, key> O(v);
	for (auto it = array_verdex.begin(); it != array_verdex.end(); ++it) {
		if (*it == O) {
			it->Erase_Edge(v);
			it->Add_Edge(v, w);
		}
	}
	Zmienna<Typ, key> Destin(v);
	for (auto it = array_verdex.begin(); it != array_verdex.end(); ++it) {
		if (*it == Destin)
			it->Erase_Edge(o);
	}
}

template<typename Typ, typename key>
inline void Graf_skierowany_listy<Typ, key>::RemoveVerdex(Typ o)
{

}

template<typename Typ, typename key>
inline void Graf_skierowany_listy<Typ, key>::RemoveEdge(key e)
{
}


